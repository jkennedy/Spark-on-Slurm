# Spark on Slurm

This repo contains all the modules and scripts that are needed to make instantiating a spark cluster in a slurm jobs easy.

## Installing 

There are three basic components:

1. Spark itself:

    I simply took the tarball from the spark home page, unpacked it and copied the contents to our shared storage where we place our code/tools which are provided by the modules.

    I didn't include the spark code in the repo - just grab a version that works for you from the spark home page.

2. The module file:

    The module file (modulefile) sets up the environment for spark, appending the paths to the executables etc.
    If you use moudles you should be able to use this, with a few small modifications. 	

    The module files sets the SPARK_CONF_DIR - this is needed if you want to set up a secure spark cluster (denying access from other users)

    Note there is a dependency on jdk (we provide this via a module also and so I added a "prereq" to the module file)

3. Helper scripts (to start the spark cluster):

    There are two helper scripts. One to create the spark cluster (/spark-common/bin/spark-start) and one to secure the cluster (/spark-common/bin/spark-create-secure-setup).
 
    These two scripts should be copied to the spark-common/bin/ dir which is defined in the module file.
    (ensure that they are world readable/executable).



# Example:

### Running a simple Spark Job via Slurm:

The following slurm.cmd file can be used to create a spark cluster within a slurm job and run a small example application.

1. Define the batch job (parameters, these will be different on each system)

2. Load the modules (jdk, spark and python/anaconda in this case) 

3. Start the Spark cluster (using the spark-start helper script)

4. Submit an application to the spark cluster using spark-submit.
   In this case the application is a simple python word count and the input data is a simple "file:" on our shared storage.   




```
#!/bin/bash
#SBATCH -N 1
#SBATCH -t 00:10:00
#SBATCH --mem 20000
#SBATCH --ntasks-per-node 4
#SBATCH --cpus-per-task 5
#SBATCH -p general 

module load jdk
module load anaconda
module load spark/2.1.0

echo ""
echo " Starting the Spark Cluster "
echo ""

spark-start

echo $MASTER

echo ""
echo " About to run the spark job"
echo ""

spark-submit --total-executor-cores 20 --executor-memory 5G /u/jkennedy/Spark/example-wordcount.py file:///u/jkennedy/Spark/Data/top10-project-gutenberg.txt 
```


### Securing the spark cluster

To secure a spark cluster a helper script (spark-common/bin/spark-create-secure-setup) has been provided.

The script creates a shared secret and trust/key stores to secure communication between the nodes in the spark cluster.

The user only needs to run this script once - the Spark configuration file (which is stored in ~/.spark-config/spark-defaults.conf ) is then used to ensure all clusters that a user creates use these values.

The config can be deleted to remove the security (drop back to an open cluster)

If you have things set up in a module

module load 
spark-create-secure-setup


Note: The SSL communication between nodes may be overkill, the shared secret may well be enough to secure a cluster. In this case the '''ssl''' options can be removed from the ~/.spark-config/spark-defaults.conf file (leaving just the basics which provide the shared secret).



